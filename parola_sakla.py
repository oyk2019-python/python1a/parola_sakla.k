parolalar = []                                                  # parolalar adında bir liste oluşturduk
while True:                                                     # Doğru olmak koşuluyla bir döögü açtık
    print('Bir işlem seçin')                                    # işlem seçtirmek için ekrana yazdırdık
    print('1- Parolaları Listele')
    print('2- Yeni Parola Kaydet')
    islem = input('Ne Yapmak İstiyorsun :')                     # kullanıcıdan ne yapmak istediğini girmesii istedik
    if islem.isdigit():                                         # eğer girilen islem int mi str mi dedik ki str girilen devam edilsin
        islem_int = int(islem)                                  # islem_int'i kullanıcıdan girilen islem'e eşitledik
        if islem_int not in [1, 2]:                             # eğer islem_int 1,2 girilmediyse asagıda hatalı islem yazdırdık
            print('Hatalı işlem girişi')
            continue                                            # bu if koşulunu burada bitirip alt if koşuluna gittik
        if islem_int == 2:                                      # eğer girilen islem_int eşitse 2'ye eşitse if koşuluna gir

            girdi_ismi = input('Bir girdi ismi ya da web sitesi adresi girin :') # kullanıcıdana girdi_ismi istedik

            kullanici_adi = input('Kullanici Adi Girin :')

            parola = input('Parola :')
            parola2 = input('Parola Yeniden :')
            eposta = input('Kayitli E-posta :')
            gizlisorucevabi = input('Gizli Soru Cevabı :')
            if kullanici_adi.strip() == '':                      # eğer kullanıcı adı boşluk girdiyse alta geç ekrana yazdır
                print('kullanici_adi girmediz')
                continue                                         # bu if koşulunu burada bitirip alt if koşuluna gittik
            if parola.strip() == '':
                print('parola girmediz')
                continue
            if parola2.strip() == '':
                print('parola2 girmediz')
                continue
            if eposta.strip() == '':
                print('eposta girmediz')
                continue
            if gizlisorucevabi.strip() == '':
                print('gizlisorucevabi girmediz')
                continue
            if girdi_ismi.strip() == '':
                print('Girdi ismi girmediz')
                continue
            if parola2 != parola:
                print('Parolalar eşit değil')
                continue

            yeni_girdi = {                                       # yeni_girdi adında dick(sözlük) oluşturduk
                'girdi_ismi': girdi_ismi,
                'kullanici_adi': kullanici_adi,
                'parola': parola,
                'eposta': eposta,
                'gizlisorucevabi': gizlisorucevabi,
            }
            parolalar.append(yeni_girdi)                         # yukarıda belirlediğimiz parolalar listesine yeni_girdi dick'i ekledik
            continue                                             # bu if koşulunu burada bitirip alt elif koşuluna gittik
        elif islem_int == 1:                                     # eğer girilen islem_int eşitse 1'e eşitse if koşuluna gir
            alt_islem_parola_no = 0                              # alt_..... adında veriable belirtip 0' eşitledik
            for parola in parolalar:                             # Parola, parolaların içinde ise for döngüsüne gir
                alt_islem_parola_no += 1                         # döngü her döndüğünde alt_.... değişkenini bir arttır
                print('{parola_no} - {girdi}'.format(parola_no=alt_islem_parola_no, girdi=parola.get('girdi_ismi'))) # parola_no ve girdi adında anahtar belirtip ekrana yazdırdık
            alt_islem = input('Yukarıdakilerden hangisi ?: ')    # kullanıcıdan islem istedik
            if alt_islem.isdigit():                              # eğer alt_islem int mi str mi cevirmesine uğraşmadan içine girdirdi
                if int(alt_islem) < 1 and len(parolalar) - 1 < int(alt_islem): # alt islem küçükse 1den ve parolaların uzunluğunu bir azaltıp alt islemden küçükse koşula gir
                    print('Hatalı parola seçimi')
                    continue                                     # hatalı seçim sonucu bitirdik koşulu alta geçtik
                parola = parolalar[int(alt_islem) - 1]           # parolayı parolalar içindeki listeye alt_islemi bir azalttık
                print('{kullanici}\n{parola}\n{gizli}\n{eposta}'.format( # format ile ekrana tekrar yazdırdık
                    kullanici=parola.get('kullanici_adi'),
                    parola=parola.get('parola'),
                    eposta=parola.get('eposta'),
                    gizli=parola.get('gizlisorucevabi'),
                ))
                continue                                         # programı bitirdik.

    print('Hatalı giriş yaptınız')
